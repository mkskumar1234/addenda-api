let User = require("../../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

class AuthController {

    async register(req, res) {
        const { first_name, last_name, email, password } = req.body;
        try {
            let userExist = await User.findOne({ email }).lean();
            if (userExist) {
                return res.status(200).json({ msg: 'Email address alredy exists.' });
            }
            let user = new User();
            user.first_name = first_name;
            user.last_name = last_name;
            user.email = email;
            user.password = password;

            user.save();
            return res.status(200).json({ msg: 'User registerd successfully' });
        } catch (error) {
            return res.status(400).json({ msg: LANG.COMMON_ERROR })
        }
    }

    async login(req, res) {
        const { email } = req.body;
        try {
            let checkUser = await User.findOne({ email }).lean();
            if (!checkUser) {
                return res.status(200).json({ msg: 'User not exists with this email id' });
            }
            if (!checkUser.status) {
                return res.status(200).json({ msg: 'Your account is deactivated by admin' });
            }
            // check user password
            if (!bcrypt.compareSync(req.body.password, checkUser.password)) {
                return res.status(200).json({ msg: 'Invalid login details.' });
            }
            // login token set here
            const { password, ...userWithOurPass } = checkUser;
            const token = jwt.sign({ sub: checkUser._id }, process.env.SECRET_KEY)

            return res.status(200).json({ data: { token, ...userWithOurPass }, msg: 'LogedIn successfully.' });

        } catch (error) {
            console.log("--error", error)
            return res.status(400).json({ msg: LANG.COMMON_ERROR })
        }

    }
}


module.exports = new AuthController();