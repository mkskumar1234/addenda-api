const express = require("express"),
    router = express.Router(),
    AuthController = require("./AuthController");

router.post("/register", FUNC.validateApi(validRules.register), AuthController.register);

router.post("/login", FUNC.validateApi(validRules.login), AuthController.login);

module.exports = router;