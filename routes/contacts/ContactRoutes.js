const express = require("express"),
    router = express.Router(),
    Contact = require("./ContactController");

router.get("/", Contact.list);

router.post("/", FUNC.validateApi(validRules.addContact), Contact.add);

router.get("/:_id", Contact.view);

router.put("/:_id", FUNC.validateApi(validRules.addContact), Contact.update);

router.delete("/:_id", Contact.delete);

module.exports = router;