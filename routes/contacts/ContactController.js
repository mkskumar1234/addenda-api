const Contact = require("../../models/Contact");
const { ObjectId } = require("mongodb");

class ContactController {

    async list(req, res) {
        const { userId } = req.body;
        let { page, keywords } = req.query;
        let query = { user_id: ObjectId(userId), status: true };
        if (!page) {
            page = 0;
        }
        if (keywords) {
            query = { ...query, name: { "$regex": new RegExp((keywords).trim(),'i')} };
        }
        let limit = parseInt(process.env.CONTACT_LIMIT || 10, 10);
        let offset = page * limit;
        try {
            console.log("--query", query)
            const result = await Contact.find(query)
                .skip(offset)
                .limit(limit)
                .sort({ name: 1 })
                .exec();

            return res.status(200).json({ data: result, msg: 'Contact list' })
        } catch (error) {
            return res.status(400).json({ msg: LANG.COMMON_ERROR })
        }
    }

    async add(req, res) {
        const { name, phone, address, userId } = req.body;
        try {
            let contact = new Contact();
            contact.user_id = ObjectId(userId);
            contact.name = name;
            contact.phone = phone;
            contact.address = address;

            contact.save();

            return res.status(200).json({ msg: 'Contact added succesfully.' })
        } catch (error) {
            return res.status(400).json({ msg: LANG.COMMON_ERROR })
        }
    }

    async view(req, res) {
        const { _id } = req.params;
        const { userId } = req.body;
        try {
            const result = await Contact.findOne({ _id, user_id: ObjectId(userId) });

            return res.status(200).json({ data: result, msg: 'Contact details.' })
        } catch (error) {
            console.log("--error", error)
            return res.status(400).json({ msg: LANG.COMMON_ERROR })
        }
    }

    async update(req, res) {
        const { _id } = req.params;
        console.log("--req.body", req.body);
        const { name, phone, address, userId } = req.body;
        try {
            const checkContact = await Contact.findOne({ _id, user_id: ObjectId(userId) });
            if (!checkContact) {
                return res.status(200).json({ msg: 'Contact not exists with this.' })
            }

            checkContact.name = name;
            checkContact.phone = phone;
            checkContact.address = address;

            checkContact.save();

            return res.status(200).json({ msg: 'Contact updated succesfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.status(400).json({ msg: LANG.COMMON_ERROR })
        }
    }

    async delete(req, res) {
        const { _id } = req.params;
        const { userId } = req.body;
        try {
            const checkContact = await Contact.findOne({ _id, user_id: ObjectId(userId) });
            if (!checkContact) {
                return res.status(200).json({ msg: 'Contact not exists with this.' })
            }

            checkContact.status = false;
            checkContact.save();

            return res.status(200).json({ msg: 'Contact archived successfully.' })
        } catch (error) {
            console.log("--error", error)
            return res.status(400).json({ msg: LANG.COMMON_ERROR })
        }
        return res.json({ dele: 1 })
    }
}

module.exports = new ContactController();