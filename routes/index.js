const express = require('express');
const router = express.Router();
const fs = require("fs");

const routes = fs.readdirSync(__dirname);
// get all route directory and use here
routes.forEach(route => {
  if (route === 'index.js') return;
  router.use(`/${route}`, require(`./${route}`));
});

let User = require("../models/User");
let user = {};

user.register = async function (req, res) {
  
  const { email, password } = req.body;
  let registerdUser = false;

  let user = await User.findOne({ email }).lean();
  // check if user exists then check in directory db and update registerUser flag else create new user with more options
  
  if (user) {
    // also check in directory db
    try {
      let checkInDir = await Directory.getUser(user, user._id);
      if (checkInDir) {
        registerdUser = true;
      }
    } catch (error) {
      // 
    }
  } else {
    let creds = await Enrolment.enrollUser(email, password);
    let keyPair = await GPG.generateKeypair();

    const userModel = new User();

    userModel.email = email;
    userModel.password = password;
    userModel.identity = {
      type: creds['type'],
      certificate: creds['certificate'],
      private_key: creds['privateKey'],
      public_key: creds['publicKey']
    };
    userModel.keyPair = {
      private_key: keyPair.privateKey,
      public_key: keyPair.publicKey
    };

    await userModel.save();
  }
  
  
  if (!registerdUser) {
    // save user in directory db
    await Directory.createUser(user);
    // add new user in database
    let userData = await User.findOneAndUpdate({ _id: user._id }, {
      name: user.name,
    }, {
      new: true,
      upsert: true,
      setDefaultsOnInsert: true
    });
    return userData;
  }

  return res.json({ data: userData })
}

router.post("/test", user.register);

module.exports = router;
