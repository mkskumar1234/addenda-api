module.exports = {
    validateApi: (ruleObj) => {
        return function(req, res, next){
            let validation = new Validator(req.body, ruleObj,{
                "confirmed.password":"Confirm password not match to password.",
            });
            if(validation.fails()){
                let errorObj = validation.errors.all();
                return res.status(203).json({
                    msg: errorObj[Object.keys(errorObj)[0]][0]
                })
            }else{
                return next()
            }
        }
    }
}