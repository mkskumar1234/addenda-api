const mongoose = require("mongoose"), 
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;

const ContactSchema = new Schema({
    user_id:{
        type: ObjectId,
        ref: 'User'
    },
    name: {
        type: String,
        trim: true,
        required: true
    },
    phone: {
        type: String,
        trim: true,
        required: true
    },
    address: {
        type: String,
        trim: true,
        required: true
    },
    status: {
        type: Boolean,
        default: true
    },
    __v: {
        type: Number,
        select: false
    }
})

ContactSchema.index({name:1, user_id: 1});

module.exports = mongoose.model("Contact", ContactSchema);