const mongoose = require("mongoose"),
    Schema = mongoose.Schema;
    bcrypt = require("bcrypt"),
    saltRound = 10;

const UserSchema = new Schema({
    first_name: {
        type: String,
        trim: true,
        required: true
    },
    last_name: {
        type: String,
        trim: true,
        required: true
    },
    email: {
        type: String,
        trim: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    status: {
        type: Boolean,
        default: true
    },
    __v:{
        type: Number,
        select : false
    }
})

UserSchema.index({email:1});

UserSchema.pre("save", async function(next){
    console.log("--this",this);
    if(!this.isModified('password')) return next();

    try {
        this.password = await bcrypt.hash(this.password, saltRound);
        next();
    } catch (error) {
        next(error)
    }
})

module.exports = mongoose.model("User", UserSchema);