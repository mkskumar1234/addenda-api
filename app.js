const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require("mongoose");
const dontenv = require("dotenv");
const jwt = require(process.cwd() + "/_helpers/jwt");
// global function
global.FUNC = require("./_helpers/func");
const errorHandler = require(process.cwd() + '/_helpers/error-handler.js');

// Validation Rules
global.Validator = require('validatorjs');
global.validRules = require("./validation_rules.js").rules;

let app = express();
dontenv.config();
// mongodb connection
mongoose.connect(process.env.MONGO_URI, {
  /* other options */
  useUnifiedTopology: true,
  useNewUrlParser: true
})
// mongoose debug on
mongoose.set("debug", process.env.NODE_ENV === 'development');

if (process.env.NODE_ENV === 'development') {
  app.use(logger('dev'));
}

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(function (req, res, next) {
  if (req.headers['authorization']) {
    console.log("--headers", req.headers['authorization']);
    req.headers['authorization'] = 'Bearer ' + req.headers['authorization'];
  }

  switch (req.headers['accept-language']) {
    case 'en':
      global.LANG = require("./locals/en/Messages").Messages;
      break;
    // we can add more langage prefences file here

    // default localizatin is english
    default:
      global.LANG = require("./locals/en/Messages").Messages;
      break;
  }
  global.JWT_TOKEN_PREFIX = process.env.JWT_TOKEN_PREFIX;
  next();
})


app.use(jwt());

app.use("/", require("./routes"));

// error handler
app.use(errorHandler);


module.exports = app;
